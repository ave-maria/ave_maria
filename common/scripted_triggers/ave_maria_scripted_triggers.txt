﻿####################################
### AVE MARIA SCRIPTED TRIGGERS ###
###################################
#
# Put any triggers here that are for general use
#

## Checks to see if this scope does not use knights
# Does not require localisation currently
does_not_use_knights_trigger = {
    OR = {
        faith = { religion_tag = islam_religion }
        culture = { this = culture:greek }
        uses_roman_govt = yes
    }
}

## Checks required perk for extort_subjects_decision
can_extort_subjects_trigger = {
    OR = {
        has_perk = it_is_my_domain_perk
		has_perk = basileus_ingenious_fiscal_loopholes_perk
    }
}

## Checks required perk for commission_epic_decision
can_commission_epic_trigger = {
    OR = {
        has_perk = writing_history_perk
        has_perk = basileus_dynastic_glorification_perk
    }
}

## Checks whether crown authority laws can be kept or passed
can_keep_or_pass_crown_authority_laws = {
    uses_roman_govt = no 
	uses_islamic_empire_govt = no
}

## Checks whether a Ave Maria government type is used
uses_ave_maria_government_type = {
    uses_roman_govt = yes
    uses_islamic_empire_govt = yes
}